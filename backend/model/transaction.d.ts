
export interface ITransaction {
    _id : string,
    account_id : string,
    transaction_code : string,
    transaction_type : "recurring" | "one_time" ,
    payment_type : "cash" | "cheque" | "credit_card",
    status : "not_paid" | "paid" | "refunded" | "partially_refunded",
    date_paid : Date,
    amount : Number,
    remark : string, 
}
