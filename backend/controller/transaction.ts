
const fs = require('fs');
import express from 'express';
import { ITransaction } from '../model/transaction';

function isTransaction (input: any): string[] {

    const schema: Record<keyof ITransaction, string> = {
        _id : 'string',
        account_id : 'string',
        transaction_code : 'string',
        transaction_type : 'string',
        payment_type : 'string',
        status : 'string',
        date_paid : 'string',
        amount : 'Number',
        remark : 'string',
    };

    const missingProperties = Object.keys(schema)
        .filter(key => input[key] === undefined)
        .map(key => key as keyof ITransaction)
        .map(key => `Missing ${key} of type ${schema[key]}`);

    return missingProperties;
}

async function getTransactions (_req: express.Request, res: express.Response, next: express.NextFunction) {
    try{
        const transactions:[ITransaction] = JSON.parse(fs.readFileSync('./files/transaction.json', 'utf8'));
        res.status(200).json({ success : true, transactions });
    } catch (err) {
        next(err);
    }
}

async function postTransactions (req: express.Request, res: express.Response, next: express.NextFunction) {
    try{
        let transaction:ITransaction = req.body;
        const typeMatch = isTransaction(transaction);
        console.log('@TYPE - ', typeMatch);
        if(typeMatch.length > 0){
            res.status(201).json({ success : false, error : typeMatch });
        } else {
            res.status(200).json({ success : true, transaction });
        }
    } catch (err) {
        next(err);
    }
}



module.exports = { getTransactions, postTransactions}