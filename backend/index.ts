
import express from 'express';
import morgan from 'morgan';
const { getTransactions, postTransactions } = require('./controller/transaction')
const bodyParser = require('body-parser')
const cors = require("cors");
const app = express();
const PORT =  3000;
const helmet = require('helmet');

var corsOptions = {
    origin: 'http://localhost:3001',
    optionsSuccessStatus: 200,
    credentials: true,
}

app.use(cors(corsOptions));

app.use(helmet());
app.use(morgan('short'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/health-check', function(_req: express.Request, res: express.Response, _next: express.NextFunction) {
    res.status(200).json({
        msg: 'Pomelo Pay API v1',
    });
})
.get('/transactions', getTransactions)
.post('/transactions', postTransactions);

app.listen(PORT);
console.log(`App started on port ${PORT}`);

module.exports = { app }; 
