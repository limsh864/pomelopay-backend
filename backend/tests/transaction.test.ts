const { app } = require('../index')
const request = require('supertest')


test('Post incomplete transaction', async (done) => {
    const res = await request(app)
        .post('/transactions')
        .send({
        "_id": "2",
        "account_id" : "123",
        "transaction_code" : "1",
        "transaction_type" : "one_time",
        "status" : "paid",
        "date_paid" : "2019-11-16T13:00:00Z",
        "remark" : null
    })
    expect(res.statusCode).toEqual(201);
    expect(res.body.success).toBe(false);
    expect(res.body.error.length).toBeGreaterThan(0);
    done();
})

test('Post complete transaction', async (done) => {
    const res = await request(app)
        .post('/transactions')
        .send({
            "_id": "1",
            "account_id": "123",
            "transaction_code": "1",
            "transaction_type": "recurring",
            "payment_type": "cash",
            "status": "paid",
            "date_paid": "2019-11-16T12:00:00Z",
            "amount": 500,
            "remark": "Total paid in cash"
        })
    expect(res.statusCode).toEqual(200);
    expect(res.body.success).toBe(true);
    done();
})

test('Get list of transaction', async (done) => {
    const res = await request(app)
        .get('/transactions');
    expect(res.statusCode).toEqual(200);
    expect(res.body.success).toBe(true);
    expect(res.body.transactions.length).toBeGreaterThan(0);
    done();
})

