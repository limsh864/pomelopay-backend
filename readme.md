

For server-side :
1) Navigate into /backend
2) Run 'npm install'
3) Run 'npm run dev'
4) Run 'npm run test' to run unit tests

For web :
1) Navigate into /frontend
2) Run 'npm install'
3) Run 'npm run start'
