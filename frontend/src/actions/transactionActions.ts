import axios from "axios";
import { GET_TRANSACTIONS } from '../Constants'

export const getTransactions = () => {
  return(dispatch : any)=>{
    axios.get(`http://localhost:3000/transactions`)
      .then(response => {
        dispatch({
          type: GET_TRANSACTIONS,
          transactions : response.data.transactions
        });
      });
  }
};