import React from 'react';
import moment from 'moment'
import { connect } from "react-redux";
import './App.css';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import { getTransactions } from './actions/transactionActions'
import _ from 'lodash';

type Props = {
  transactionStore?: any
}

type ConnectProps = {
  getTransactions: () => void;
};

type State = {
  acc_id: any;
  t_code: any;
  t_type: any;
  p_type: any;
  status: any;
  amount: any;
  date: any;
  remark: any;
};

type ActualProps = Props & ConnectProps

class App extends React.Component<ActualProps, State> {
  constructor(props: ActualProps) {
    super(props);
    this.state = {
      t_code:"asc",
      acc_id:"asc",
      t_type: "asc",
      p_type: "asc",
      status: "asc",
      amount: "asc",
      date: "asc",
      remark: "asc",
    };
  }

  componentDidMount(){
    this.props.getTransactions();
  }
  render() {
    let { transactions } = this.props.transactionStore;
    
    return (
        <Paper className="root">
        <Table className="table" aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell sortDirection={this.state.acc_id}>
              <TableSortLabel direction={this.state.acc_id} onClick={()=>{ this.setState({ acc_id : this.state.acc_id === 'asc' ? 'desc' : 'asc' })}}>
                Account ID
              </TableSortLabel>
              </TableCell>
              <TableCell align="right" sortDirection={this.state.t_code}>
              <TableSortLabel direction={this.state.t_code} onClick={()=>{ this.setState({ t_code : this.state.t_code === 'asc' ? 'desc' : 'asc' })}}>
                Transaction Code
              </TableSortLabel>
              </TableCell>
              <TableCell align="right" sortDirection={this.state.t_type}>
              <TableSortLabel direction={this.state.t_type} onClick={()=>{ this.setState({ t_type : this.state.t_type === 'asc' ? 'desc' : 'asc' })}}>
              Transaction Type
              </TableSortLabel>
              </TableCell>
              <TableCell align="right" sortDirection={this.state.p_type}>
              <TableSortLabel direction={this.state.p_type} onClick={()=>{ this.setState({ p_type : this.state.p_type === 'asc' ? 'desc' : 'asc' })}}>
              Payment Type
              </TableSortLabel>
              </TableCell>
              <TableCell align="right" sortDirection={this.state.status}>
              <TableSortLabel direction={this.state.status} onClick={()=>{ this.setState({ status : this.state.status === 'asc' ? 'desc' : 'asc' })}}>
              Status
              </TableSortLabel>
              </TableCell>
              <TableCell align="right" sortDirection={this.state.amount}>
              <TableSortLabel direction={this.state.amount} onClick={()=>{ this.setState({ amount : this.state.amount === 'asc' ? 'desc' : 'asc' })}}>
              Amount
              </TableSortLabel>
              </TableCell>
              <TableCell align="right" sortDirection={this.state.date}>
              <TableSortLabel direction={this.state.date} onClick={()=>{ this.setState({ date : this.state.date === 'asc' ? 'desc' : 'asc' })}}>
              Date Paid
              </TableSortLabel>
              </TableCell>
              <TableCell align="right" sortDirection={this.state.remark}>
              <TableSortLabel direction={this.state.remark} onClick={()=>{ this.setState({ remark : this.state.remark === 'asc' ? 'desc' : 'asc' })}}>
              Remark
              </TableSortLabel>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
          {(() => {
            if(transactions && transactions.length > 0){
              let { t_code, acc_id, t_type, p_type, status, amount, date, remark } = this.state
              transactions = _.orderBy(transactions, 
                ['account_id', 'transaction_code','transaction_type','payment_type','status','amount','date_paid','remark'],
                [acc_id, t_code , t_type, p_type, status, amount, date, remark]);
              return transactions.map((row:any) => (
                <TableRow key={row._id}>
                  <TableCell component="th" scope="row">{row.account_id}</TableCell>
                  <TableCell align="right">{row.transaction_code}</TableCell>
                  <TableCell align="right">{row.transaction_type}</TableCell>
                  <TableCell align="right">{row.payment_type}</TableCell>
                  <TableCell align="right">{row.status}</TableCell>
                  <TableCell align="right">{row.amount}</TableCell>
                  <TableCell align="right">{moment(row.date_paid).toISOString()}</TableCell>
                  <TableCell align="right">{row.remark}</TableCell>
                </TableRow>
              ))
            }
          })()}
            
          </TableBody>
          </Table>
        </Paper>
    );
  }
}

const mapStateToProps = (state: any) => {
  return { ...state }
};

const mapDispatchToProps = (dispatch: any) => ({
  getTransactions: () => dispatch(getTransactions())
})

export default connect<ConnectProps>(
  mapStateToProps,
  mapDispatchToProps
)(App)


