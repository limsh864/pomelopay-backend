import { combineReducers, Reducer} from 'redux';
import transactionStore from './transactionReducer';

// All reducers combined

export interface ApplicationState {
    transactionStore: any
}

export const reducers: Reducer<ApplicationState> = combineReducers<ApplicationState>({
    transactionStore
});

