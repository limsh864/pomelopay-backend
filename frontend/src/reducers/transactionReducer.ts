import { GET_TRANSACTIONS } from "../Constants";
  type Action = {
    type:string,
    transactions? : object,
  }
  
  type State = {
    transactions?:any
  }
  
  export default (state:State = {}, action: Action) => {
    switch (action.type) {
      case GET_TRANSACTIONS:
        return {
          ...state, transactions: action.transactions
        }
      default:
        return state
    }
  }
  